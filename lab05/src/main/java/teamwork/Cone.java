package teamwork;

// Elissar Fadel- 2134398


public class Cone implements Shape3d {
    //setting up the fields height and radius
    private double height;
    private double radius;
    //constructer to initiate the height and radius
    public Cone(double radius, double height){
        if(height <= 0 || radius <= 0){
            throw new IllegalArgumentException("the height and radius mus be more than 0");
        }
        this.height= height;
        this.radius= radius;
    }
    
    //the methods implemented from shape3d
    // get volume, calculates volume of cone
    // get surface area calculates surface area of cone
    public double getVolume(){
        return (1/3)*(Math.PI)*(this.radius*this.radius)*(this.height);
    }
    public double getSurfaceArea(){
        return (Math.PI)*this.radius*(this.radius + Math.sqrt((this.radius*this.radius)+(this.height*this.height)));
    }
}
