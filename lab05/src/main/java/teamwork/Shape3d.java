package teamwork;

public interface Shape3d {
    double getVolume();
    double getSurfaceArea();

}
