package teamwork;

//Hooman Afshari-2134814

public class Cylinder implements Shape3d {
    private double radius;
    private double height;
//Constructor
    public Cylinder(double radius, double height) {
    //Input validation
        if (radius<=0 || height <= 0) {
            throw new IllegalArgumentException("Values cannot be less than zero");
        }
        this.height=height;
        this.radius=radius;
    }
    //getVolume method
    public double getVolume() {

        return Math.PI*this.radius*this.radius*this.height;

    }
    //getSurface method
    public double getSurfaceArea() {

        return this.radius*Math.PI*((this.radius*this.radius)+2)*Math.PI*this.radius*this.height;

    }

}
