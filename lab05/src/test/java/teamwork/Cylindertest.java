package teamwork;
//Elissar Fadel- 2134398

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class Cylindertest {
    private static final double DELTA = 1e-15;

    @Test
    public void shouldCatchException()
    {
        try{
            Shape3d c= new Cylinder(-1, 0);
            fail();
        }
        catch(IllegalArgumentException e){
            
        }
    }

    @Test
    public void shouldGetVolumeRight()
    {
        Shape3d c= new Cylinder(3, 4);
        double result= c.getVolume();
        double expected= Math.PI*3*3*4; 

        assertEquals(result, expected, DELTA);
    }
    
    @Test
    public void shouldGetAreaRight()
    {
        Shape3d c= new Cylinder(2, 5);
        double result= c.getSurfaceArea();
        double expected= 2*Math.PI*((2*2)+2)*Math.PI*2*5; 

        assertEquals(result, expected, DELTA);
    }
}
