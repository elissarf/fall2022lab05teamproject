//Hooman Afshari - 2134814

package teamwork;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class ConeTest {
    @Test
    public void testConstructor() {
        try  {
            Shape3d cone = new Cone(0,-1);
            fail();
        }

        catch (IllegalArgumentException e) {

        }
    }
    @Test
    public void testVolumeCone() {
        Shape3d cone = new Cone (2,5);
        double result = cone.getVolume();
        double expected = (1/3)*(Math.PI)*(2*2)*(5);
        assertEquals(expected, result, 0.001);

    }
    @Test
    public void testSurfaceAreaCone() {
        Shape3d cone = new Cone (2,5);
        double result = cone.getSurfaceArea();
        double expected = (Math.PI)*(2)*(2+Math.sqrt((2*2)+(5*5)));
        assertEquals(expected, result, 0.001);

    }
}
